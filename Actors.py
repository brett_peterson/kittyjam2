import pygame
import math
import AnimatedSprite
from GameData import TEAM
from SoundMachine import instance as SM
from GameData import instance as GD

class MOVEMENT_STATE(object):
    IDLE = 0
    WALKING_UP = 1
    WALKING_RIGHT = 2
    WALKING_DOWN = 3
    WALKING_LEFT = 4
    SPAWNING = 5 # correspond to spawn animation row in spritesheets for animation
    DYING = 6


class DIRECTION(object):
    UP = 1
    RIGHT = 2
    DOWN = 3
    LEFT = 4


class Actor:
    def __init__(self, rect, spriteData, map):
        self.map_ref = map
        self.rect = rect
        self.animation = AnimatedSprite.AnimatedSprite(spriteData)
        self.anim_num = 0
        self.movement_state = MOVEMENT_STATE.IDLE
        self.xdir = DIRECTION.RIGHT
        self.animation.set_animation(self.anim_num)
        self.move_vec = [0,0]
        self.egg_image = pygame.image.load('data/egg.png')
        self.egg_width = 16
        self.egg_height = 24

        # Characteristics of actors:
        self.canAttack = True
        self.runningSpeed = 10
        self.carrySpeed = 6
        self.canCarryEgg = True
        self.canCarryIce = True
        self.is_carrying_an_egg = False
        self.team = TEAM.BLUE
        self.alive = True


    def update(self):
        # Apply movement
        if self.move_vec[0] != 0 or self.move_vec[1] != 0:
            # store a copy of our x/y direction so we can roll
            # back the position if there is a collision
            collide_x = False
            collide_y = False

            # TODO: add carry logic to adjust speed
            speed = self.runningSpeed

            dx = self.move_vec[0] * speed
            if dx > 0:
                dx_int = math.ceil(dx)
            else:
                dx_int = math.floor(dx)

            dy = self.move_vec[1] * speed
            if dy > 0:
                dy_int = math.ceil(dy)
            else:
                dy_int = math.floor(dy)

            temp_rect = self.rect.move(dx_int, 0)
            # check collisions in x direction
            if (self.map_ref.collide_rect(temp_rect) == True) or \
                    (temp_rect.left < 0) or (temp_rect.right > 1919):
                collide_x = True

            # update our x position and the temp rect
            if (collide_x == True):
                temp_rect = self.rect.move(-dx_int, 0)

            temp_rect.move_ip(0, dy_int)
            # check collisions in y direction
            if (self.map_ref.collide_rect(temp_rect) == True) or \
                    (temp_rect.top < 0) or (temp_rect.bottom > 1079):
                collide_y = True

            # update our y position
            if (collide_y == True):
                temp_rect.move_ip(0, -dy_int)

            self.rect.centerx = temp_rect.centerx
            self.rect.centery = temp_rect.centery

        self.update_movement_state()
        self.animation.update()

    def render(self, surface):
        # if self.move
        # pygame.draw.rect(surface, pygame.Color(255, 0, 0), self.rect, 4)

        if not self.alive:
            return
        flip = self.xdir != DIRECTION.RIGHT
        self.animation.render(surface, self.rect, flip)

        if self.is_carrying_an_egg:
            surface.blit(self.egg_image, (self.rect.left + ((self.rect.width * 3) / 4), self.rect.top, self.egg_width, self.egg_height))

    def move(self, vec):
        if vec[0] != 0 and vec[1] != 0:
            # we are tryting to move in 2 directions at once!
            if self.move_vec[0] == vec[0] or self.move_vec[1] == vec[1]:
                # one of our previous movements is still being applied, change nothing
                pass
            else:
                # something has changed, prioritize x movement i guess
                if vec[0] != 0:
                    self.move_vec[0] = vec[0]
                    self.move_vec[1] = 0
                else:
                    self.move_vec[0] = 0
                    self.move_vec[1] = vec[1]
        else:
            self.move_vec = vec
        # self.update_movement_state(vec)

    def update_movement_state(self):
        nonlooping_states = [
            MOVEMENT_STATE.SPAWNING, MOVEMENT_STATE.DYING
        ]
        if self.movement_state in nonlooping_states:
            return

        if self.move_vec[0] < 0:
            new_state = MOVEMENT_STATE.WALKING_LEFT
        elif self.move_vec[0] > 0:
            new_state = MOVEMENT_STATE.WALKING_RIGHT
        elif self.move_vec[1] < 0:
            new_state = MOVEMENT_STATE.WALKING_UP
        elif self.move_vec[1] > 0:
            new_state = MOVEMENT_STATE.WALKING_DOWN
        else:
            new_state = MOVEMENT_STATE.IDLE
        self.set_movement_state(new_state)

    def set_movement_state(self, state_to_set):
        if self.movement_state == state_to_set:
            return

        self.movement_state = state_to_set
        self.animation.set_animation(self.movement_state)


class TRex(Actor):
    def __init__(self, map, team, team_index):
        spawn_key = "B"
        if team == TEAM.RED:
            spawn_key = "R"
        # map_spawn_points are 1-indexed
        spawn_key += ("%d" % (team_index + 1))
        self.spawn_point = map.get_spawn_locations(spawn_key)
        print("TRex:ctr:team:%s:team_idx:%d:spawnkey:%s:spawnpt:%s" % (
            team,
            team_index,
            spawn_key,
            self.spawn_point
        ))
        
        if team == TEAM.BLUE:
            super().__init__(
                pygame.Rect(self.spawn_point[0], self.spawn_point[1], 64, 64),
                'data/trex_blue_sprite.json', 
                map
            )
        else:
            super().__init__(
                pygame.Rect(self.spawn_point[0], self.spawn_point[1], 64, 64),
                'data/trex_red_sprite.json', 
                map
            )

            #pygame.Rect(self.spawn_point[0], self.spawn_point[1], 48, 64),
            #'data/warrior_cat.json',
            #map
        
        self.try_to_spawn()
        SM().play_afx("SPAWN")
        self.olddir = None
        self.dir = DIRECTION.RIGHT

        # T-Rex attributes
        self.canAttack = True
        self.runningSpeed = 8
        self.carrySpeed = 4
        self.canCarryEgg = True

    def spawn_finish_callback(self):
        print("SpawnFinish Called!")
        self.movement_state = MOVEMENT_STATE.IDLE
        self.animation.set_animation(
            self.movement_state
        )

    def try_to_spawn(self):
        self.alive = GD().request_respawn(self.team)
        print("Try to Spawn called for %s: %s" % (
            self, self.alive
        ))
        if not self.alive:
            return
        self.movement_state = MOVEMENT_STATE.SPAWNING
        self.animation.set_animation(
            self.movement_state,
            self.spawn_finish_callback
        )

    def wait_to_respawn(self):
        self.x = self.spawn_point[0]
        self.y = self.spawn_point[1]
        self.rect = pygame.Rect(
            self.spawn_point[0],
            self.spawn_point[1],
            48, 64
        )
        self.try_to_spawn()


    def die(self):
        print("DIE called! on %s" % self)
        self.movement_state = MOVEMENT_STATE.DYING
        self.animation.set_animation(
            self.movement_state,
            self.wait_to_respawn
        )

    def set_team(self, team):
        self.team = team

    def dir2vec(self, d):
        from pygame.math import Vector2
        if d == MOVEMENT_STATE.WALKING_LEFT:
            return Vector2(-1, 0)
        elif d == MOVEMENT_STATE.WALKING_RIGHT:
            return Vector2(1, 0)
        elif d == MOVEMENT_STATE.WALKING_UP:
            return Vector2(0, -1)
        elif d == MOVEMENT_STATE.WALKING_DOWN:
            return Vector2(0, 1)
        else:
            return Vector2(0, 0)
            # raise Exception("unknown dir: " + d)

    def dir2frontcenter(self):
        r = self.rect
        v = r.center
        if self.movement_state == MOVEMENT_STATE.WALKING_LEFT:
            v = r.midleft
        elif self.movement_state == MOVEMENT_STATE.WALKING_RIGHT:
            v = r.midright
        elif self.movement_state == MOVEMENT_STATE.WALKING_UP:
            v = r.midtop
        elif self.movement_state == MOVEMENT_STATE.WALKING_DOWN:
            v = r.midbottom
        return v


    def resolve_collisions(self, rex_list):
        invincible_states = [
            MOVEMENT_STATE.SPAWNING, MOVEMENT_STATE.DYING
        ]
        if self.movement_state in invincible_states:
            return
        for other_rex in rex_list:
            if other_rex.movement_state in invincible_states:
                continue
            if other_rex == self:
                continue
            # if other_rex.team == self.team:
            #     continue
            if self.rect.colliderect(other_rex.rect):
                self_rex_vec  = self.dir2vec(self.movement_state)
                other_rex_vec = self.dir2vec(other_rex.movement_state)
                if -1 * self_rex_vec == other_rex_vec:
                    self.rect.move_ip(      self_rex_vec * -80)
                    other_rex.rect.move_ip(other_rex_vec * -80)
                else:
                    # todo: kill
                    print("KILL>KILL>KILL")
                    self_frontcenter = self.dir2frontcenter()
                    if other_rex.rect.collidepoint(self_frontcenter):
                        print("- Self got OtherRex")
                        other_rex.die()
                        continue

                    other_frontcenter = other_rex.dir2frontcenter()
                    if self.rect.collidepoint(other_frontcenter):
                        print("- OtherRex got Self")
                        self.die()
                        continue




class Arrow(object):
    def __init__(self, spriteData):
        self.rect = pygame.Rect(0, 0, 64, 64)
        self.movement_state = MOVEMENT_STATE.IDLE
        self.animation = AnimatedSprite.AnimatedSprite(spriteData)
        self.olddir = None
        self.dir = DIRECTION.RIGHT

    def update(self):
        if self.dir != self.olddir:
            self.animation.set_animation(self.dir)

        self.animation.update()

    def render(self, surface):
        self.animation.render(surface, self.rect)


class RedArrow(Arrow):
    def __init__(self):
        super().__init__('data/red_arrow_sprite.json')


class BlueArrow(Arrow):
    def __init__(self):
        super().__init__('data/blue_arrow_sprite.json')

