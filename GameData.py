

class TEAM:
    BLUE = 0
    RED = 1
    NONE = 3


class _GameData(object):
    def __init__(self):
        self.team_a = []
        self.team_b = []

        self.blue_team_eggs = 6
        self.red_team_eggs = 6
        self.unclaimed_eggs = 6
        self.egg_locations = []
        self.winning_team = TEAM.NONE

    def set_player_teams(self, pteams):
        for idx, t in enumerate(pteams):
            if t == -1:
                self.team_a.append(idx)
            elif t == 1:
                self.team_b.append(idx)
            else:
                print("ERROR: Player has invalid team assignment")

    def steal_egg(self, team):
        if team == TEAM.RED:
            if self.blue_team_eggs > 0:
                print("RED stole egg from BLUE")
                self.blue_team_eggs -= 1
                return True
        elif team == TEAM.BLUE:
            if self.red_team_eggs > 0:
                self.red_team_eggs -= 1
                print("BLUE stole egg from RED")
                return True
        return False

    def claim_egg(self, team):
        if team == TEAM.RED:
            print("Claimed egg for team RED")
            self.red_team_eggs += 1
        elif team == TEAM.BLUE:
            print("Claimed egg for team BLUE.")
            self.blue_team_eggs += 1

    def request_egg_for_map(self):
        can_add_egg_to_map = False
        if self.unclaimed_eggs > 0:
            self.unclaimed_eggs -= 1
            can_add_egg_to_map = True

        return can_add_egg_to_map

    # Call during gameplay - do not call at the outset of gameplay, we just assume
    # the initial "hatched" eggs are freebies.
    def request_respawn(self, team):
        can_spawn = False

        if team == TEAM.BLUE:
            if self.blue_team_eggs > 0:
                self.blue_team_eggs -= 1
                can_spawn = True
            else:
                self.winning_team = TEAM.RED
                can_spawn = True

        if team == TEAM.RED:
            if self.red_team_eggs > 0:
                self.red_team_eggs -= 1
                can_spawn = True
            else:
                self.winning_team = TEAM.BLUE
                can_spawn = False

        return can_spawn

    # Checks for win conditions and returns the current game condition.
    def get_game_win_condition(self):
        return self.winning_team != TEAM.NONE


_instance = _GameData()


def instance():
    return _instance

