import os
import pygame.mixer as mix


# ===================================================================
# SoundMachine class: It will manage pygame.mixer and
#                     pygame.mixer.music to play sound effects and
#                     background music
# ===================================================================
class _SoundMachine(object):
    def __init__(self):
        self.sounds = {}
        self.tracks = {}
        self.tracks["TITLE"] = "titleScreen.ogg"
        self.tracks["GAME_BG"] = "GameTheme.ogg"
        self.tracks["CREDITS"] = "guitar_riff.ogg"
        self.tracks["PLAYER_SELECTION"] = "playerSelect.ogg"

    def load_sounds(self):
        # This function should be called after pygame.mixer.init()
        # It maps all of the sound effects to "Keys"
        self.load_sound(os.path.join("data", "attack.wav"), "ATTACK")
        self.load_sound(os.path.join("data", "claim.wav"), "EGG_CLAIM")
        self.load_sound(os.path.join("data", "steal.ogg"), "EGG_STEAL")
        self.load_sound(os.path.join("data", "bump.wav"), "BUMP")
        self.load_sound(os.path.join("data", "rawr.ogg"), "SPAWN")
        self.load_sound(os.path.join("data", "game_over.wav"), "GAME_OVER")
        self.load_sound(os.path.join("data", "panda.ogg"), "PANDA")

        for key, s in self.sounds.items():
            s.set_volume(0.2)
        
    def load_sound(self, file, key):
        try:
            sound = mix.Sound(file)
        except Exception as e:
            print("Failed to load: " + file + str(e))
        else:
            if sound:
                self.sounds[key] = sound
                
    def play_afx(self, key):
        if key in self.sounds:
            self.sounds[key].play()
    
    def start_music(self, song, loop=True):
        if song in self.tracks:
            try:
                mix.music.load(os.path.join("data", self.tracks[song]))
            except:
                print("Failed to load: " + self.tracks[song])
            else:
                print("playing: " +  self.tracks[song])
                if loop:
                    mix.music.play(-1)
                else:
                    mix.music.play(0)

    def stop_music(self):
        mix.music.fadeout(200)

    
_instance = _SoundMachine()


def instance():
    return _instance
