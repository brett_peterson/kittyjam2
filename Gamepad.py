import os
import json
import pygame

class _BtnMap(object):
    def __init__(self, jname):
        self.bmap = []

        # set default values for joypad buttons
        self.bmap.append({"axis_inverted": "no", 0: "A", 1: "B", 2: "X", 3: "Y", 4: "L", 5: "R", 6: "SE", 7: "ST"})
        
        # load input config file or create a new one
        fpath = os.path.join("data", jname + "_config.json")
        try:
            with open(fpath, 'r+') as f:
                self.bmap = json.load(f)

        except IOError:
            # No input config file, write defaults to the file
            with open(fpath, 'w') as f:
                json.dump(self.bmap, f, indent=2)
                
    def get_mapped_btn(self, event):
        btn = None
        if str(event.button) in self.bmap[0].keys():
            btn = self.bmap[0][str(event.button)]
        return btn

    def get_axis_inverted(self):
        return self.bmap[0]["axis_inverted"] == True

# ===================================================================
class _Gamepad(object):
    def __init__(self):
        self.gamepads = []
        
        # set up joysticks
        pygame.joystick.init()
        print("num joysticks: " + str(pygame.joystick.get_count()))

        for j in range(pygame.joystick.get_count()):
            pygame.joystick.Joystick(j).init()
            joy_name = pygame.joystick.Joystick(j).get_name().replace(" ", "_")

            # the last element in a gamepads entry is the last movement vector (for tap detection)
            self.gamepads.append((pygame.joystick.Joystick(j), _BtnMap(joy_name), [0,0]))

    def _get_snapped_axes(self, event):
        if event.type == pygame.JOYAXISMOTION:
            x = self.gamepads[event.joy][0].get_axis(0)
            y = self.gamepads[event.joy][0].get_axis(1)

            if self.gamepads[event.joy][1].get_axis_inverted():
                y = -1 * y

        elif event.type == pygame.JOYHATMOTION:
            x, y = event.value
            # hat is inverted on my controller for some reason, if we find issues later, we can add it
            # to the config file
            y = 0-y
        else:
            return None, None

        if x < -0.2:
            x = -1
        elif x > 0.2:
            x = 1
        else:
            x = 0

        if y < -0.2:
            y = -1
        elif y > 0.2:
            y = 1
        else:
            y = 0

        return x, y

    def get_num_pads(self):
        return len(self.gamepads)

    def get_mapped_btn(self, event):
        btn = None
        if event.joy < len(self.gamepads):
            btn = self.gamepads[event.joy][1].get_mapped_btn(event)
        return btn

    def get_dir_tap(self, event):
        if event.type == pygame.JOYAXISMOTION or \
            event.type == pygame.JOYHATMOTION:
            ret_v = [0,0]
            xin, yin = self._get_snapped_axes(event)
            last_dir = self.gamepads[event.joy][2]

            # check x axis
            if last_dir[0] == 0 and xin != 0:
                ret_v[0] = xin
            last_dir[0] = xin

            # check y axis
            if last_dir[1] == 0 and yin != 0:
                ret_v[1] = yin
            last_dir[1] = yin
            return ret_v

    def get_move_vec(self, event):
        x, y = self._get_snapped_axes(event)
        return [x,y]


_instance = _Gamepad()

def instance():
    return _instance
