import pygame
import os
from SoundMachine import instance as SM
from Gamepad import instance as GP
from Map import GameMap
from Actors import *
from GameData import instance as GD
from pygame.math import Vector2
from itertools import cycle
import time

BLINK_EVENT = pygame.USEREVENT + 0

FADE_IN_TIME = 1
UNFADED_TIME = 3
FADE_OUT_TIME = 1
FADE_IN_EASING = lambda x: x  # Linear
FADE_OUT_EASING = lambda x: x  # Linear

ST_FADEIN = 0
ST_UNFADED = 1
ST_FADEOUT = 2

# ===================================================================
class SceneBase(object):
    def __init__(self):
        self.next = self

    def process_input(self, events):
        for event in events:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_2:
                    SM().stop_music()
                    self.switch_to_scene(Credits())
                elif event.key == pygame.K_3:
                    SM().stop_music()
                    self.switch_to_scene(TitleScene())
                elif event.key == pygame.K_4:
                    SM().stop_music()
                    self.switch_to_scene(GameScene())
                elif event.key == pygame.K_5:
                    SM().stop_music()
                    self.switch_to_scene(PlayerSelection())
                elif event.key == pygame.K_ESCAPE:
                    SM().stop_music()
                    pygame.event.post(pygame.event.Event(pygame.QUIT))

    def update(self):
        print("uh-oh, you didn't override this in the child class")

    def render(self, surface):
        print("uh-oh, you didn't override this in the child class")

    def switch_to_scene(self, next_scene):
        self.next = next_scene

# ===================================================================
class StudioScene(SceneBase):
    def __init__(self):
        SceneBase.__init__(self)
        pygame.display.set_caption('')
        self.state = ST_FADEIN
        self.last_state_change = time.time()
        self.alpha = 0
        self.background = Background('data/slow_panda_1.bmp', [0, 0])

        SM().play_afx("PANDA")

    def process_input(self, events):
        super().process_input(events)
        for event in events:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.event.post(pygame.event.Event(pygame.QUIT))

                if event.key == pygame.K_SPACE:
                    self.switch_to_scene(TitleScene())

            if event.type == pygame.JOYBUTTONDOWN:
                btn = GP().get_mapped_btn(event)
                if btn:
                    self.switch_to_scene(TitleScene())

    def update(self):
        ## Update the state
        state_time = time.time() - self.last_state_change

        if self.state == ST_FADEIN:
            if state_time >= FADE_IN_TIME:
                self.state = ST_UNFADED
                state_time -= FADE_IN_TIME
                self.last_state_change = time.time() - state_time

        elif self.state == ST_UNFADED:
            if state_time >= FADE_IN_TIME:
                self.state = ST_FADEOUT
                state_time -= UNFADED_TIME
                self.last_state_change = time.time() - state_time

        elif self.state == ST_FADEOUT:
            if state_time >= FADE_OUT_TIME:
                self.switch_to_scene(TitleScene())

        else:
            raise ValueError()

        if self.state == ST_FADEIN:
            self.alpha = FADE_IN_EASING(1.0 * state_time / FADE_IN_TIME)
        elif self.state == ST_UNFADED:
            self.alpha = 1
        elif self.state == ST_FADEOUT:
            self.alpha = 1. - FADE_OUT_EASING(1.0 * state_time / FADE_OUT_TIME)
        else:
            raise ValueError()

    def render(self, surface):
        surface.fill((255, 255, 255))

        fade_surface = pygame.surface.Surface((surface.get_width(), surface.get_height()))
        fade_surface.set_alpha(255 * self.alpha)
        fade_surface.blit(self.background.image, (0, 0))
        surface.blit(fade_surface, (0, 0))

# ===================================================================
class TitleScene(SceneBase):
    def __init__(self):
        SceneBase.__init__(self)

        # load Fonts
        title_font = pygame.font.Font("data/african.ttf", 24)
        pygame.display.set_caption('')
        welcome_text_on = title_font.render("Press any button to start!", True, (255, 255, 255))
        blink_rect = welcome_text_on.get_rect()
        off_text_surface = pygame.Surface(blink_rect.size)
        self.blink_surfaces = cycle([welcome_text_on, off_text_surface])
        self.blink_surface = next(self.blink_surfaces)

        self.time = pygame.time.get_ticks()

        self.background = Background('data/eggstinction.png', [0, 0])

        # start title music
        SM().start_music("TITLE")

    def process_input(self, events):
        super().process_input(events)
        for event in events:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.event.post(pygame.event.Event(pygame.QUIT))

                if event.key == pygame.K_SPACE:
                    self.switch_to_scene(PlayerSelection())

            if event.type == pygame.JOYBUTTONDOWN:
                btn = GP().get_mapped_btn(event)
                if btn:
                    SM().stop_music()
                    self.switch_to_scene(PlayerSelection())

    def update(self):
        new_time = pygame.time.get_ticks()
        if self.time + 1200 < new_time:
            self.time = new_time
            self.blink_surface = next(self.blink_surfaces)

    def render(self, surface):
        # clear the screen - White
        surface.fill((255, 255, 255))
        surface.blit(self.background.image, self.background.rect)

        # render static text
        x, y = surface.get_size()
        surface.blit(self.blink_surface, (((x - self.blink_surface.get_width()) / 2, (y - 44))))


# ===================================================================
class DemoScene(SceneBase):
    def __init__(self):
        SceneBase.__init__(self)

        # load Fonts
        title_font = pygame.font.Font(None, 30)
        pygame.display.set_caption('')
        self.welcome_text = title_font.render("Press any button to start!", True, (0, 0, 0))

        # create title scene objects
        self.actors = [
            TRex(), TRex()
        ]
        self.actors[0].rect.top = 250
        self.actors[1].rect.topleft = (250, 250)
        self.actors[1].dir = DIRECTION.LEFT
        self.velocity = [
            Vector2(0, 0),
            Vector2(0, 0),
        ]

        # start title music
        SM().start_music("TITLE")

    def process_input(self, events):
        super().process_input(events)
        for event in events:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.event.post(pygame.event.Event(pygame.QUIT))

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    self.velocity[0] = Vector2(0, -4)
                elif event.key == pygame.K_DOWN:
                    self.velocity[0] = Vector2(0, 4)
                elif event.key == pygame.K_LEFT:
                    self.velocity[0] = Vector2(-4, 0)
                elif event.key == pygame.K_RIGHT:
                    self.velocity[0] = Vector2(4, 0)

            if event.type == pygame.KEYUP:
                if event.key == pygame.K_UP or \
                   event.key == pygame.K_DOWN:
                   self.velocity[0][1] = 0
                if event.key == pygame.K_LEFT or \
                   event.key == pygame.K_RIGHT:
                   self.velocity[0][0] = 0

                if event.key == pygame.K_SPACE:
                    self.switch_to_scene(PlayerSelection())

            if event.type == pygame.JOYBUTTONDOWN:
                btn = GP().get_mapped_btn(event)
                if btn:
                    self.switch_to_scene(PlayerSelection())

    def update(self):
        def dir2vec(d):
            if d == DIRECTION.LEFT:
                return Vector2(-1, 0)
            elif d == DIRECTION.RIGHT:
                return Vector2(1, 0)
            elif d == DIRECTION.UP:
                return Vector2(0, -1)
            elif d == DIRECTION.DOWN:
                return Vector2(0, 1)
            else:
                raise Exception("unknown dir: " + d)

        trex, trex2 = self.actors
        if trex.rect.colliderect(trex2.rect):
            v1, v2 = (dir2vec(trex.dir), dir2vec(trex2.dir))
            if -1 * v1 == v2:
                trex.rect.move_ip(v1 * -80)
                trex2.rect.move_ip(v2 * -80)
            else:
                # todo: kill
                trex.rect.left = 0
                trex2.dir = (trex2.dir + 1) % 4

        if trex.movement_state == MOVEMENT_STATE.SPAWNING:
            return # NO CONTROL RESPONSE DURING SPAWNING !!!

        trex.rect.move_ip(self.velocity[0])
        if self.velocity[0].magnitude_squared() > 0:
            trex.movement_state = MOVEMENT_STATE.WALKING
        else:
            trex.movement_state = MOVEMENT_STATE.IDLE

        dx = self.velocity[0][0]
        if dx > 0:
            trex.dir = DIRECTION.RIGHT
        elif dx < 0:
            trex.dir = DIRECTION.LEFT

        dy = self.velocity[0][1]
        if dy > 0:
            trex.dir = DIRECTION.DOWN
        elif dy < 0:
            trex.dir = DIRECTION.UP

        for a in self.actors:
            a.update()


    def render(self, surface):
        # clear the screen - White
        surface.fill((255, 255, 255))

        for a in self.actors:
            a.render(surface)

        # render static text
        x, y = surface.get_size()
        surface.blit(self.welcome_text, (((x - self.welcome_text.get_width()) / 2, (y / 4))))


# ===================================================================
class PlayerSelection(SceneBase):
    def __init__(self):
        SceneBase.__init__(self)
        SM().start_music("PLAYER_SELECTION")
        # create game objects
        self.player_positions = [0]*GP().get_num_pads()
        self.player_text = []
        f = pygame.font.Font(None, 30)
        self.team_blue_text = f.render("BLUE TEAM", True, (0x2d, 0x8f, 0xf0))
        self.team_red_text = f.render("RED TEAM", True, (0xf5, 0x31, 0x31))
        self.background = Background('data/background.png', [0, 0])
        self.trex_red = pygame.image.load('data/trex_red.png')
        self.trex_blue = pygame.image.load('data/trex_blue.png')

        for i in range(len(self.player_positions)):
            f = pygame.font.Font(None, 30)
            self.player_text.append(f.render("PLAYER " + str(i), True, (255, 255, 255)))

    def process_input(self, events):
        super().process_input(events)
        for event in events:
            if event.type == pygame.JOYAXISMOTION or \
                event.type == pygame.JOYHATMOTION:
                dir = GP().get_dir_tap(event)
                if dir[0] == -1 and self.player_positions[event.joy] > -1:
                    self.player_positions[event.joy] -= 1
                elif dir[0] == 1 and self.player_positions[event.joy] < 1:
                    self.player_positions[event.joy] += 1
            if event.type == pygame.JOYBUTTONDOWN:
                if "ST" == GP().get_mapped_btn(event) and 0 not in self.player_positions:
                    GD().set_player_teams(self.player_positions)
                    SM().stop_music()
                    self.switch_to_scene(GameScene())

    def update(self):
        pass

    def render(self, surface):
        # surface.fill((0, 0, 0))
        surface.blit(self.background.image, self.background.rect)
        x, y = surface.get_size()

        # render team titles
        x_title_blue = ((x - self.team_blue_text.get_width()) / 2) - 200
        x_title_red = ((x - self.team_red_text.get_width()) / 2) + 200
        surface.blit(self.team_blue_text, (x_title_blue, (y / 4) - 100))
        surface.blit(self.team_red_text, (x_title_red, (y / 4) - 100))
        surface.blit(self.trex_red, (x_title_red, (y / 4) - 220))
        surface.blit(self.trex_blue, (x_title_blue, (y / 4) - 220))

        # render team selections
        y_player = y/4
        for idx, pos in enumerate(self.player_positions):
            x_player = (x - self.player_text[idx].get_width()) / 2

            if pos == -1:
                xpos = x_player - 200
            elif pos == 1:
                xpos = x_player + 200
            else:
                xpos = x_player
            surface.blit(self.player_text[idx], (xpos, y_player))
            y_player += y/8


# ===================================================================
class GameScene(SceneBase):
    def __init__(self):
        SceneBase.__init__(self)

        # create game objects
        self.game_map = GameMap()
        self.players = {}

        print("GameScene:init:team_a:%s" % GD().team_a)
        print("GameScene:init:team_b:%s" % GD().team_b)
        team_index = 0
        for p in (GD().team_a + GD().team_b):
            if p == len(GD().team_a):
                team_index = 0
            team = TEAM.BLUE
            if p >= len(GD().team_a):
                team = TEAM.RED
            self.players[p] = TRex(self.game_map, team, team_index)
            team_index += 1

        title_font = pygame.font.Font(None, 30)
        self.welcome_text = title_font.render("This the game screen!", True, (0, 0, 0))

        SM().start_music("GAME_BG")

    def process_input(self, events):
        super().process_input(events)
        nonresponsive_states = [
            MOVEMENT_STATE.SPAWNING, MOVEMENT_STATE.DYING
        ]
        for e in events:
            if e.type == pygame.JOYAXISMOTION or \
                         pygame.JOYHATMOTION:
                # if self.players[e.joy].movement_state in nonresponsive_states:
                #     continue
                vec = GP().get_move_vec(e)
                if len(self.players.items()) > 0:
                    if None not in vec:
                        self.players[e.joy].move(vec)
            if e.type == pygame.JOYBUTTONDOWN:
                # if self.players[e.joy].movement_state in nonresponsive_states:
                #     continue
                if "A" == GP().get_mapped_btn(e):
                    # print("A Pressed!", self.players[e.joy].team, self.players[e.joy].is_carrying_an_egg)
                    if self.game_map.is_on_enemy_nest(self.players[e.joy]):
                        # print("ENEMY NEST! ", self.players[e.joy].team, self.players[e.joy].is_carrying_an_egg)
                        if not self.players[e.joy].is_carrying_an_egg:
                            if GD().steal_egg(self.players[e.joy].team):
                                SM().play_afx("EGG_STEAL")
                                self.players[e.joy].is_carrying_an_egg = True
                    if self.game_map.is_on_home_nest(self.players[e.joy]):
                        # print("HOME NEST!", self.players[e.joy].team, self.players[e.joy].is_carrying_an_egg)
                        if self.players[e.joy].is_carrying_an_egg:
                            SM().play_afx("EGG_CLAIM")
                            GD().claim_egg(self.players[e.joy].team)
                            self.players[e.joy].is_carrying_an_egg = False

    def update(self):
        # update players
        for key, value in self.players.items():
            value.update()

        # check for win condition
        if GD().get_game_win_condition() == True:
            SM().stop_music()
            SM().play_afx("GAME_OVER")
            self.switch_to_scene(Credits())

        player_list = []
        for joystick_index, player in self.players.items():
            player_list.append(player)
        for player in player_list:
            player.resolve_collisions(player_list)

    def render(self, surface):
        self.game_map.render(surface)

        # render players
        for key, value in self.players.items():
            if value.alive:
                value.render(surface)

# ===================================================================
class Credits(SceneBase):
    def __init__(self):
        SceneBase.__init__(self)
        SM().start_music("CREDITS", False)
        pygame.display.set_caption('End credits')
        self.trex_img = pygame.image.load(os.path.join("data", "trex.png"))
        self.font = pygame.font.SysFont("Arial", 40)
        self.screen_rect = pygame.Rect(0,0,1920,1056)
        self.credit_list = [
            "- CREDITS -", "KittyJam - Big Dino Battle", "",
            "Shame Dept", "Head - Danielle Deboer", "",
            "Others involved? Randos", "",
            "Kevin Imber", "Brett Peterson", "Steve Grover",
            "Trevor Peterson", "Matt Rodger", "Seth Pellegrino",
            "Bradley Nelson", "Tommy Kooi"
        ]
        self.texts = []
            # we render the text once, since it's easier to work with surfaces
            # also, font rendering is a performance killer
        for i, line in enumerate(self.credit_list):
            s = self.font.render(line, 1, (255, 215, 0))
            # we also create a Rect for each Surface.
            # whenever you use rects with surfaces, it may be a good idea to use sprites instead
            # we give each rect the correct starting position
            r = s.get_rect(centerx=(self.screen_rect.centerx * 3/2 - 70), y=self.screen_rect.bottom + i * 45)
            self.texts.append((r, s))

    def process_input(self, events):
        super().process_input(events)
        for event in events:
            # Handle keyboard events -----------------
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.event.post(pygame.event.Event(pygame.QUIT))
                elif event.key == pygame.K_RETURN:
                    SM().stop_music()
                    self.switch_to_scene(TitleScene())


    def update(self):
        for r, _ in self.texts:
            r.move_ip(0, -1)

        if not self.screen_rect.collidelistall([r for (r, _) in self.texts]):
            SM().stop_music()
            self.switch_to_scene(TitleScene())

    def render(self, screen):
        screen.fill((0, 0, 0))
        render_rect = pygame.Rect(40, 40, 707, 1000)
        screen.blit(pygame.transform.scale(self.trex_img, (render_rect.w, render_rect.h)), render_rect)

        for r, s in self.texts:
            screen.blit(s, r)

class Background(pygame.sprite.Sprite):
    def __init__(self, image_file, location):
        pygame.sprite.Sprite.__init__(self)  #call Sprite initializer
        self.image = pygame.image.load(image_file)
        self.rect = self.image.get_rect()
        self.rect.left, self.rect.top = location
