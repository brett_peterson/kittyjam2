# KittyJam2

# Troubleshooting Setup

### Mac: Superslow Framerate on Higher Resolution Monitors

Pygame by default uses SDL 1, which performs terribly on macs using higher resolution monitors. You will actually see framerates of < 10/second.

In order to get around this, you'll need to compile pygame yourself pointing to a local install of SDL 2. It sounds scary, but it's really just running these commands ([source](https://www.pygame.org/wiki/MacCompile?parent=#pygame%20with%20sdl2)):

```console
brew install sdl2 sdl2_gfx sdl2_image sdl2_mixer sdl2_net sdl2_ttf
git clone https://github.com/pygame/pygame.git
cd pygame
python setup.py -config -auto -sdl2
python setup.py install
```

Be careful of later installing dependencies that need pygame, they might potentially install over your local compiled version. If that happens, you'll need to re-run these steps. This came up in the 2019 jam when `pytmx` was added to the dependency list

