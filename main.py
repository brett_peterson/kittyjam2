import os
import sys
import pygame
import SoundMachine
import Scene

# DEFINE here if you want the screen scaled down
scaled = False

# ------------------------------------------------------ #
# set up the game window, pygame modules, and load media
# ------------------------------------------------------ #
position = 100, 100
os.environ['SDL_VIDEO_WINDOW_POS'] = str(position[0]) + "," + str(position[1])

# set up audio mixer
pygame.mixer.pre_init(44100, -16, 2, 2048)
pygame.mixer.init()
SoundMachine.instance().load_sounds()

pygame.init()

# set up the game display
size = width, height = 1920, 1080
screen = pygame.Surface(size, pygame.DOUBLEBUF | pygame.HWSURFACE)
scaledScreen = pygame.display.set_mode((int(width * 0.5), int(height * 0.5)) if scaled else size, pygame.DOUBLEBUF | pygame.HWSURFACE | pygame.FULLSCREEN)

clock = pygame.time.Clock()

# create the active scene and initialize it to the title scene
active_scene = Scene.StudioScene()

# flush events - this was done to prevent some issues with joysticks
pygame.event.clear()

# load font for FPS display
fps_font = pygame.font.Font(None, 15)

# ------------------------------------------------------ #
# Start main game loop
# ------------------------------------------------------ #
running = True
while running:
    # limit frame rate to 60 fps
    clock.tick(60)
    
    # -------------------------------------------------- #
    # Handle all user input
    # -------------------------------------------------- #
    player_events = []
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

        if event.type == pygame.KEYUP or \
            event.type == pygame.KEYDOWN or \
            event.type == pygame.JOYAXISMOTION or \
            event.type == pygame.JOYHATMOTION or \
            event.type == pygame.JOYBUTTONUP or \
            event.type == pygame.JOYBUTTONDOWN or \
            event.type == pygame.MOUSEBUTTONUP or \
            event.type == pygame.MOUSEBUTTONDOWN or \
            event.type == pygame.MOUSEMOTION:

            # send input events to the player's input handler
            player_events.append(event)
    
    # process player events
    active_scene.process_input(player_events)
    
    # -------------------------------------------------- #
    # update active scene
    # -------------------------------------------------- #
    active_scene.update()
    
    # -------------------------------------------------- #
    # Render
    # -------------------------------------------------- #
    active_scene.render(screen)

    # debug - show FPS
    fps_text = fps_font.render("FPS: "+str(clock.get_fps()), True, (200, 200, 200))
    # screen.blit(fps_text, (5, 5))
    # end debug

    scaledScreen.blit(pygame.transform.scale(screen, (int(width * 0.5), int(height * 0.5)) if scaled else size), (0, 0))

    pygame.display.update()
    
    # -------------------------------------------------- #
    # switch to the next scene
    # -------------------------------------------------- # 
    active_scene = active_scene.next
    
# shutdown
pygame.quit()
sys.exit()
