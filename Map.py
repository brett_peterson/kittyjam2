import pygame


from pytmx import *
from pytmx.util_pygame import load_pygame
from GameData import instance as GD
from GameData import TEAM

class GameMap(object ):
        def __init__(self):
            GD()
            tm = load_pygame('data/kitty_jam_2_map_1.tmx')
            self.tmx_data = tm

            self.spawn_loc = {}
            obj_layer = tm.get_layer_by_name("spawn")
            for obj in obj_layer:
                self.spawn_loc[obj.name] = [obj.x, obj.y]

            print(self.spawn_loc)

            self.team_red_nest = Nest((1820,980), TEAM.RED)
            self.team_blue_nest = Nest((0, 0), TEAM.BLUE)
            self.egg_image = pygame.image.load('data/egg.png')


        def render(self, surface):
            surface.fill((120, 22, 75))
            x, y = surface.get_size()
            for layer in self.tmx_data.visible_layers:
                if isinstance(layer, TiledTileLayer):
                    self.render_tile_layer(surface, layer, (0,0))

            # TODO: move to update function?
            self.team_red_nest.number_of_eggs = GD().red_team_eggs
            self.team_blue_nest.number_of_eggs = GD().blue_team_eggs
            self.team_red_nest.render(surface)
            self.team_blue_nest.render(surface)

            for egg_location in GD().egg_locations:
                surface.blit(self.egg_image, (egg_location))

        def render_tile_layer(self, surface, layer, origin):
            # deref these heavily used references for speed
            tw = self.tmx_data.tilewidth
            th = self.tmx_data.tileheight
            surface_blit = surface.blit
            ox, oy = origin

            # iterate over the tiles in the layer
            for x, y, image in layer.tiles():
                surface_blit(image, ((x * tw) - ox, (y * th) - oy))

        def collide_rect(self, rect):
            x_min = int(rect.x / self.tmx_data.tilewidth)
            x_max = int((rect.x + rect.w) / self.tmx_data.tilewidth)
            y_min = int(rect.y / self.tmx_data.tileheight)
            y_max = int((rect.y + rect.h) / self.tmx_data.tileheight)

            if x_min < 0:
                x_min = 0
            if y_min < 0:
                y_min = 0

            if x_max >= self.tmx_data.width:
                x_max = self.tmx_data.width - 1
            if y_max >= self.tmx_data.height:
                y_max = self.tmx_data.height - 1

            for x_ind in range(x_min, x_max + 1):
                for y_ind in range(y_min, y_max + 1):
                    p = self.tmx_data.get_tile_properties(x_ind, y_ind, 0)
                    if p is not None:
                        if 'collision' in p:
                            if p['collision'] == 'true':
                                return True
            return False

        # player example : "B1"
        def get_spawn_locations(self, player):
            return self.spawn_loc[player]

        def is_on_enemy_nest(self, actor):
            if actor.team == TEAM.RED:
                if actor.rect.colliderect(self.team_blue_nest.nest_rect):
                    return True
            else:
                if actor.rect.colliderect(self.team_red_nest.nest_rect):
                    return True
            return False

        def is_on_home_nest(self, actor):
            if actor.team == TEAM.RED:
                if actor.rect.colliderect(self.team_red_nest.nest_rect):
                    return True
            elif actor.team == TEAM.BLUE:
                if actor.rect.colliderect(self.team_blue_nest.nest_rect):
                    return True
            return False


class Nest():
    def __init__(self, location, team):
        self.team = team
        self.nest = pygame.image.load('data/nest.png')
        self.egg_height = 24
        self.egg_width = 16
        self.nest_width = 100
        self.nest_edge = location[0] + self.nest_width
        self.egg = pygame.image.load('data/egg.png')
        self.nest_location = location
        self.number_of_eggs = 10
        self.nest_rect = pygame.Rect(location[0], location[1], 100, 100)
        print("Nest Rect: ", (location[0], location[1], self.egg_width, self.egg_height))

    def get_rect(self):
        return self.nest.get_rect()

    def add_egg(self):
        self.number_of_eggs += 1

    def remove_egg(self):
        self.number_of_eggs -= 1

    def render(self, surface):
        surface.blit(self.nest, self.nest_location)
        current_Egg_X = self.nest_location[0]
        current_Egg_Y = self.nest_location[1]
        for x in range( 0, self.number_of_eggs):
            if current_Egg_X > self.nest_edge:
                current_Egg_X = self.nest_location[0]
                current_Egg_Y += self.egg_height
            surface.blit(self.egg, (current_Egg_X, current_Egg_Y))
            current_Egg_X = current_Egg_X + self.egg_width + 10
