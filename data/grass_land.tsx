<?xml version="1.0" encoding="UTF-8"?>
<tileset name="grass_land" tilewidth="16" tileheight="16" tilecount="288" columns="24">
 <image source="buch-outdoor.png" width="384" height="192"/>
 <terraintypes>
  <terrain name="Grass" tile="174"/>
  <terrain name="dirt" tile="100"/>
  <terrain name="water" tile="172"/>
 </terraintypes>
 <tile id="0" terrain="0,0,0,1"/>
 <tile id="1" terrain="0,0,1,1"/>
 <tile id="2" terrain="0,0,1,1"/>
 <tile id="3" terrain="0,0,1,1"/>
 <tile id="4" terrain="0,0,1,1"/>
 <tile id="5" terrain="0,0,1,0"/>
 <tile id="24" terrain="0,1,0,1"/>
 <tile id="25" terrain="1,1,1,0"/>
 <tile id="26" terrain="1,1,0,1"/>
 <tile id="29" terrain="1,0,1,0"/>
 <tile id="48" terrain="0,1,0,1"/>
 <tile id="49" terrain="1,0,1,1"/>
 <tile id="50" terrain="0,1,1,1"/>
 <tile id="53" terrain="1,0,1,0"/>
 <tile id="72" terrain="0,1,0,1"/>
 <tile id="73" terrain="0,1,1,0"/>
 <tile id="74" terrain="1,0,0,1"/>
 <tile id="77" terrain="1,0,1,0"/>
 <tile id="96" terrain="0,1,0,1"/>
 <tile id="97" terrain="1,0,0,1"/>
 <tile id="98" terrain="0,1,1,0"/>
 <tile id="100" terrain="1,1,1,1"/>
 <tile id="101" terrain="1,0,1,0"/>
 <tile id="120" terrain="0,1,0,0"/>
 <tile id="121" terrain="1,1,0,0"/>
 <tile id="122" terrain="1,1,0,0"/>
 <tile id="123" terrain="1,1,0,0"/>
 <tile id="124" terrain="1,1,0,0"/>
 <tile id="125" terrain="1,0,0,0"/>
 <tile id="144" terrain="0,0,0,2"/>
 <tile id="145" terrain="0,0,2,2"/>
 <tile id="146" terrain="0,0,2,2"/>
 <tile id="147" terrain="0,0,2,2"/>
 <tile id="148" terrain="0,0,2,2"/>
 <tile id="149" terrain="0,0,2,0"/>
 <tile id="150" terrain="0,0,0,0"/>
 <tile id="168" terrain="0,2,0,2"/>
 <tile id="169" terrain="2,2,2,0"/>
 <tile id="170" terrain="2,2,0,2"/>
 <tile id="171" terrain="2,2,2,2">
  <properties>
   <property name="collision" value="true"/>
  </properties>
 </tile>
 <tile id="172" terrain="2,2,2,2">
  <properties>
   <property name="collision" value="true"/>
  </properties>
 </tile>
 <tile id="173" terrain="2,0,2,0"/>
 <tile id="174" terrain="0,0,0,0"/>
 <tile id="192" terrain="0,2,0,2"/>
 <tile id="193" terrain="2,0,2,2"/>
 <tile id="194" terrain="0,2,2,2"/>
 <tile id="195" terrain="2,2,2,2">
  <properties>
   <property name="collision" value="true"/>
  </properties>
 </tile>
 <tile id="196" terrain="2,2,2,2">
  <properties>
   <property name="collision" value="true"/>
  </properties>
 </tile>
 <tile id="197" terrain="2,0,2,0"/>
 <tile id="198" terrain="0,0,0,0"/>
 <tile id="216" terrain="0,2,0,2"/>
 <tile id="217" terrain="0,2,2,0"/>
 <tile id="218" terrain="2,0,0,2"/>
 <tile id="219" terrain="2,2,2,2">
  <properties>
   <property name="collision" value="true"/>
  </properties>
 </tile>
 <tile id="220" terrain="2,2,2,2">
  <properties>
   <property name="collision" value="true"/>
  </properties>
 </tile>
 <tile id="221" terrain="2,0,2,0"/>
 <tile id="222" terrain="0,0,0,0"/>
 <tile id="240" terrain="0,2,0,2"/>
 <tile id="241" terrain="2,0,0,2"/>
 <tile id="242" terrain="0,2,2,0"/>
 <tile id="243" terrain="2,2,2,2">
  <properties>
   <property name="collision" value="true"/>
  </properties>
 </tile>
 <tile id="244" terrain="2,2,2,2">
  <properties>
   <property name="collision" value="true"/>
  </properties>
 </tile>
 <tile id="245" terrain="2,0,2,0"/>
 <tile id="246" terrain="0,0,0,0"/>
 <tile id="264" terrain="0,2,0,0"/>
 <tile id="265" terrain="2,2,0,0"/>
 <tile id="266" terrain="2,2,0,0"/>
 <tile id="267" terrain="2,2,0,0"/>
 <tile id="268" terrain="2,2,0,0"/>
 <tile id="269" terrain="2,0,0,0"/>
 <tile id="270" terrain="0,0,0,0"/>
</tileset>
