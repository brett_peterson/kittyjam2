import pygame
import random

# strategies...
def age(amount):
    def _age(particle):
        particle.age += amount
    return _age

def float_up(speed):
    def _float_up(particle):
        particle.y -= speed
    return _float_up

def float_dir(speed, slope):
    def _float_dir(particle):
        x_change = slope[0] * speed
        y_change = slope[1] * speed

        particle.x += x_change
        particle.y += y_change
    return _float_dir

def kill_at_age(death_age):
    def _kill_at_age(particle):
        if particle.age >= death_age:
            particle.kill()
    return _kill_at_age
    
def kill_at_height(value):
    def _kill_at_height(particle):
        if particle.y >= value:
            particle.kill()
    return _kill_at_height
    
# Particle Base - renders a circle with radius equal to size
class particle(object):
    def __init__(self, color, size, *strategies):
        self.x, self.y = 0, 0
        self.color = color
        self.alive = True
        self.age = 0
        self.strategies = strategies
        self.size = size
        
    def kill(self):
        self.alive = False
        
    def move(self):
        for s in self.strategies:
            s(self)
    
    def render(self, screen, origin=(0, 0)):
        target_pos = (origin[0] + self.x, origin[1] + self.y)
        pygame.draw.circle(screen, self.color, target_pos, self.size, 2)

class snow_particle(particle):
    def __init__(self, color, size, *strategies):
        super(snow_particle, self).__init__(color, size, *strategies)
        
    def render(self, screen, origin=(0, 0)):
        # override the render function, draw snowflakes
        target_pos = (origin[0] + self.x, origin[1] + self.y)
        if self.size < 2:
            super(snow_particle, self).render(screen, origin)
        else:
            # draw a plus
            pygame.draw.line(screen, self.color, (target_pos[0] - self.size, target_pos[1]), (target_pos[0] + self.size, target_pos[1]), 2)
            pygame.draw.line(screen, self.color, (target_pos[0], target_pos[1] - self.size), (target_pos[0], target_pos[1] + self.size), 2)
            
# Bubble factory
def BubbleBlower(color):
    def create():
        for _ in range(random.choice([0,0,0,0,0,0,0,1,1,2])):
            behaviour = float_up(random.randint(2, 5)), age(1), kill_at_age(random.randint(60*1, 60*3))
            p = particle(color, random.randint(6, 15), *behaviour)
            p.x += random.randint(-20, 20)
            yield p
    
    while True:
        yield create()
            
# Snow factory
def Snow():
    def create():
        for _ in range(random.choice([0,0,0,0,0,0,0,1,1,2])):
            behaviour = float_up(random.randint(-3, -1)), age(1), kill_at_height(1080)
            p = snow_particle((255, 255, 255), random.randint(2, 6), *behaviour)
            p.x += random.randint(0, 1920)
            yield p
    
    while True:
        yield create()

def Dust():
    def create():
        for _ in range(random.choice([3, 3, 3, 4, 4])):
            behaviour = age(1), kill_at_age(30), float_dir(1, [0, 1])
            p = particle((160, 123, 170), random.randint(2, 4), *behaviour)
            p.x += random.randint(-12, 12)
        yield p
        
    while True:
        yield create()
        
def MagicTrail(color, slope):
    def create():
        for _ in range(random.choice([1,4,5,7,9,15,10,2,4,20])):
            float_behavior = float_dir(random.randint(1, 2), slope)
            behavior = float_behavior, age(1), kill_at_age(random.randint(10, 12))
            p = particle(color, random.randint(2, 3), *behavior)
            p.x += random.randint(-10, 10)
            p.y += random.randint(-10, 10)
            yield p
    
    while True:
        yield create()
            
class Emitter(object):
    def __init__(self, pos=(0,0)):
        self.particles = []
        self.pos = pos
        self.factories = []
        self.running = False
        self.pulsed = False
        self.pulse_time = 0
        
    def add_factory(self, factory, pre_fill=0):
        self.factories.append(factory)
        tmp = []
        # pre-fill
        for _ in range(pre_fill):
            new_particles = list(next(factory))
            for p in new_particles:
                p.x += self.pos[0]
                p.y += self.pos[1]
            tmp.extend(new_particles)
            for p in tmp:
                p.move()
        self.particles.extend(tmp)
    
    def pulse(self, time_ms):
        self.running = True
        self.pulsed = True
        self.pulse_time = time_ms + pygame.time.get_ticks()
        
    def run(self):
        self.running = True
        
    def stop(self):
        self.running = False
        
    def update(self):
        # only create new particles if emitter is running
        if self.running:
            for f in self.factories:
                new_particles = list(next(f))
                for p in new_particles:
                    p.x += self.pos[0]
                    p.y += self.pos[1]
                self.particles.extend(new_particles)

        for p in self.particles[:]:
            p.move()
            if p.alive == False:
                self.particles.remove(p)
                
        if self.pulsed and pygame.time.get_ticks() > self.pulse_time:
            self.pulsed = False
            self.running = False
                
    def render(self, screen, origin=(0,0)):
        for p in self.particles:
            p.render(screen, origin)
